var gulp = require('gulp');
var sass = require('gulp-ruby-sass');

gulp.task('styles', function() { 
        sass('assets/scss/*.scss')
        .on('error', sass.logError)
        .pipe(gulp.dest('assets/css'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('assets/scss/*.scss',['styles']);
});
